import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:Ledger/helpers/database_helper.dart';

import 'package:Ledger/models/task_model.dart';

class AddTask extends StatefulWidget {
  final Function updateTaskList;
  final Task task;

  AddTask({this.updateTaskList, this.task });
  @override
  _AddTask createState() => _AddTask();
}

class _AddTask extends State<AddTask> {
  final _formkey = GlobalKey<FormState>();
  String _title = '';
  DateTime _date = DateTime.now();
  String _text = '';
  TextEditingController _dateController = TextEditingController();

  final DateFormat _dateformatter = DateFormat('dd-mm-yyyy');

  @override
  void initState() {
    super.initState();
    if (widget.task != null) {
      _title = widget.task.title;
      _date = widget.task.date;
	  _text = widget.task.text;
    }
  }

  _handleDatePicker() async {
    final DateTime date = await showDatePicker(
        context: context,
        initialDate: _date,
        firstDate: DateTime(2000),
        lastDate: DateTime(2100));
    if (date != null && date != _date) {
      setState(() {
        _date = date;
      });
      _dateController.text = _dateformatter.format(date);
    }
  }

  _delete(){
		DatabaseHelper.instance.deleteTask(widget.task.id);
		widget.updateTaskList();
		Navigator.pop(context);
  }

  _submit() {
    if (_formkey.currentState.validate()) {
      _formkey.currentState.save();
      print('s_title,s_date,s_text');

      Task task = Task(title: _title, date: _date, text: _text);
      if (widget.task == null) {
        DatabaseHelper.instance.insertTask(task);
      } else {
		task.id = widget.task.id;
        DatabaseHelper.instance.updateTask(task);
      }
      widget.updateTaskList();
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black,
        body: GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: SingleChildScrollView(
                child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 60),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        GestureDetector(
                          onTap: () => Navigator.pop(context),
                          child: Icon(
                            Icons.arrow_back,
                            size: 30,
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(height: 20),
                        Form(
                            key: _formkey,
                            child: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 20),
                                child: Column(children: <Widget>[
                                  Padding(
                                      padding:
                                          EdgeInsets.symmetric(vertical: 2),
                                      child: TextFormField(
                                        style: TextStyle(
                                            fontSize: 12,
                                            color: Colors.redAccent,
                                            fontWeight: FontWeight.bold,
                                            fontFamily: 'LiberationMono'),
                                        decoration: new InputDecoration(
                                          border: InputBorder.none,
                                          hintText: 'Title',
                                          hintStyle: TextStyle(
                                              color: Colors.redAccent),
                                        ),
                                        validator: (input) =>
                                            input.trim().isEmpty
                                                ? 'Title is needed'
                                                : null,
                                        onSaved: (input) => _title = input,
                                        initialValue: _title,
                                      )),
                                  Padding(
                                      padding:
                                          EdgeInsets.symmetric(vertical: 0),
                                      child: Row(children: <Widget>[
                                        Expanded(
                                          child: TextField(
                                            readOnly: true,
                                            controller: _dateController,
                                            style: TextStyle(
                                                fontSize: 12,
                                                color: Colors.grey,
                                                fontFamily: 'LiberationMono'),
                                            onTap: _handleDatePicker,
                                            decoration: new InputDecoration(
                                              border: InputBorder.none,
                                              hintText: 'Date',
                                              hintStyle: TextStyle(
                                                  color: Colors.white),
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                            child: Container(
                                                child: FlatButton(
                                          child: Icon(
                                            Icons.keyboard_return,
                                            size: 30,
                                            color: Colors.redAccent,
                                          ),
                                          onPressed: _submit,
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 100),
                                        )))
                                      ])),
                                  Padding(
                                      padding:
                                          EdgeInsets.symmetric(vertical: 30),
                                      child: TextFormField(
                                        style: TextStyle(
                                            fontSize: 14,
                                            color: Colors.white,
                                            fontFamily: 'LiberationMono'),
                                        keyboardType: TextInputType.multiline,
                                        maxLines: null,
                                        decoration: InputDecoration(
                                          border: InputBorder.none,
                                          hintText: 'Text',
                                          hintStyle:
                                              TextStyle(color: Colors.white),
                                        ),
                                        validator: (input) =>
                                            input.trim().isEmpty
                                                ? 'Text is needed'
                                                : null,
                                        onSaved: (input) => _text= input,
                                        initialValue: _text,
                                      )),
                                ])))
                      ],
                    )))));
  }
}
