import 'package:Ledger/helpers/database_helper.dart';
import 'package:Ledger/models/task_model.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import './add_task_screen.dart';
//					Container(
//             margin: EdgeInsets.all(10),
//           height: 100.00,
//            width: double.infinity,
//          color: Colors.red,

class LedgerHome extends StatefulWidget {
  @override
  _LedgerHome createState() => _LedgerHome();
}

class _LedgerHome extends State<LedgerHome> {
  Future<List<Task>> _taskList;

  final DateFormat _dateformatter = DateFormat('dd-mm-yyyy');
  @override
  void initState() {
    super.initState();
    _updateTaskList();
  }

  _updateTaskList() {
    setState(() {
      _taskList = DatabaseHelper.instance.getTaskList();
    });
  }

  _delete(int id) {
    DatabaseHelper.instance.deleteTask(id);
    _updateTaskList();
  }

  Widget _buildTask(Task task) {
    return Column(children: <Widget>[
      ListTile(
        title: Text(
          '''${task.title}
			  ''',
          style: TextStyle(color: Colors.redAccent),
		  maxLines: 10,
        ),
        subtitle: new Text(task.text != null ? task.text : ' ',
            style: TextStyle(color: Colors.white)),
        onTap: () => Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => AddTask(
                updateTaskList: _updateTaskList,
                task: task,
              ),
            )),
        onLongPress: () => _delete(task.id),
      ),
      Divider()
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black,
        floatingActionButton: FloatingActionButton(
            backgroundColor: Colors.redAccent,
            child: Icon(Icons.add),
            onPressed: () => Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (_) => AddTask(updateTaskList: _updateTaskList)))),
        body: FutureBuilder(
            future: _taskList,
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }

              return ListView.builder(
                  padding:
                      EdgeInsets.symmetric(vertical: 100.00, horizontal: 30),
                  itemCount: 1 + snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    if (index == 0) {
                      return Padding(
                        padding:
                            EdgeInsets.symmetric(horizontal: 0, vertical: 20.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              'notes',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 40.0,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'LiberationMono'),
                            ),
                            SizedBox(
                              height: 2,
                            ),
                            Text(
                              ' note count: ${snapshot.data.length}',
                              style: TextStyle(
                                color: Colors.redAccent,
                                fontSize: 10.00,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ],
                        ),
                      );
                    }
                    return _buildTask(snapshot.data[index - 1]);
                  });
            }));
  }
}
