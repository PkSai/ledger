import 'package:flutter/material.dart';
import './screens/LedgerHome.dart';
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Ledger',
	  debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.red,
		brightness: Brightness.dark,
		accentColor: Colors.redAccent,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: LedgerHome(), 
    );
  }
}

