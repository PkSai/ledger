class Task {
  int id;
  String title;
  DateTime date;
  String text;
  Task({this.id, this.title,  this.text,this.date});
  Task.withid({this.id, this.title, this.text, this.date });

  Map<String, dynamic> toMap() {
    final map = Map<String, dynamic>();
    if (id != null) {
      map['id'] = id;
    }
    map['title'] = title;
    map['date'] = date.toIso8601String();
    map['text'] = text;
    return map;
  }

  factory Task.fromMap(Map<String, dynamic> map) {
    return Task.withid(
        id: map['id'],
        title: map['title'],
        date: DateTime.parse(map['date']),
        text: map['text'],
        );
  }
}
