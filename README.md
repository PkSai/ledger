# Ledger

A minimalistic Notebook application with an aim to emulate a real life notebook. Emphasizes the reading and writing aspect of note taking without overwhelming the user with features that are seldom used. 

The app has two screens, the notes homescreen and the writing screen.

## Screenshots
<img src="https://gitlab.com/PkSai/ledger/-/raw/master/screenshots/s1.png" width="270" height="540">
<img src="https://gitlab.com/PkSai/ledger/-/raw/master/screenshots/s2.png" width="270" height="540">
<img src="https://gitlab.com/PkSai/ledger/-/raw/master/screenshots/s3.png" width="270" height="540">

## Dependencies
  - intl: ^0.16.0
  - path_provider: ^1.6.22
  - sqflite: ^1.3.1

## Building

**In your app directory**

```
	flutter build apk --split-per-abi
```

## Install

To install the app use

```
		flutter install
```

